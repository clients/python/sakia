#!/bin/bash

wget https://duniter.org/en/files/license_g1.txt
sed "s/:date.*//g" -i license_g1.txt
sed "s/:modified.*//g" -i license_g1.txt
md-to-html -i license_g1.txt -o src/sakia/g1_license.html
rm license_g1.txt
